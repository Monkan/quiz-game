//
//  GameGenerator.m
//  QuizGame
//
//  Created by ITHS on 2016-02-03.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import "GameGenerator.h"

@interface GameGenerator()

@property (nonatomic)NSArray *questions;

@end

@implementation GameGenerator

-(instancetype) init
{
    self = [super init];
    if (self) {
        self.questions =@[@[@"What is the main component of glass?", @"sand", @"clay", @"oil", @"water", @"sand"],
                          @[@"How many Oscars did the movie Titanic win?", @"7", @"9", @"11", @"5", @"11"],
                          @[@"What is Sake made from?", @"wheat", @"rice", @"oat", @"corn", @"rice"],
                          @[@"How long is the Great Wall of China?", @"21,196.18 km", @"10.337.23 km", @"6,795.52 km", @"16,475.81 km", @"21,196.18 km"],
                          @[@"Which is the longest river i Europe?", @"Donau", @"Seine", @"The Themse", @"Wolga", @"Wolga"],
                          @[@"What was Elvis Presley's middle name?", @"Aaron", @"Paul", @"Fred", @"George", @"Aaron"],
                          @[@"After which animal are the Canary Islands named?", @"bird", @"dog", @"turtle", @"fish", @"dog"],
                          @[@"What is the name of the Barcelona football stadium?", @"Anfield", @"San Siro", @"Camp Nou", @"Parc des Princes", @"Camp Nou"],
                          @[@"Which planet is closest to the sun?", @"Mercury", @"Saturn", @"Telus", @"Jupiter", @"Mercury"],
                          @[@"What year was Google founded?", @"2000", @"1986", @"1998", @"1992", @"1998"]];
    }
    return self;
}

-(NSArray*) randomQuestion{
    
    return self.questions [arc4random() % self.questions.count];
}

@end





