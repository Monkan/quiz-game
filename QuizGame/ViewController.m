//
//  ViewController.m
//  QuizGame
//
//  Created by ITHS on 2016-02-03.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import "ViewController.h"
#import "GameGenerator.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *questionOutput;
@property (weak, nonatomic) IBOutlet UILabel *answerOutput;
@property (strong, nonatomic)GameGenerator *gameGenerator;
@property (weak, nonatomic) IBOutlet UIButton *option1;
@property (weak, nonatomic) IBOutlet UIButton *option2;
@property (weak, nonatomic) IBOutlet UIButton *option3;
@property (weak, nonatomic) IBOutlet UIButton *option4;

@end

@implementation ViewController

-(GameGenerator*)gameGenerator {
    if (!_gameGenerator) {
        _gameGenerator = [[GameGenerator alloc] init];
    }
    return _gameGenerator;
}

- (void)viewDidLoad {
    [super viewDidLoad];
  
}

- (IBAction)questionGenerator:(id)sender {
    
    NSArray *newQuestion = [self.gameGenerator randomQuestion];

    self.questionOutput.text = [newQuestion objectAtIndex:0];
    
    [self.option1 setTitle:[newQuestion objectAtIndex:1]forState:normal];
    [self.option2 setTitle:[newQuestion objectAtIndex:2]forState:normal];
    [self.option3 setTitle:[newQuestion objectAtIndex:3]forState:normal];
    [self.option4 setTitle:[newQuestion objectAtIndex:4]forState:normal];

    
}


@end
