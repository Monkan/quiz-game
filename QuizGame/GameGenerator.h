//
//  GameGenerator.h
//  QuizGame
//
//  Created by ITHS on 2016-02-03.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameGenerator : NSObject

-(NSArray*)randomQuestion;

@end
